library(tidyverse)
library(ggplot2)
library(datasets)
library(nortest)

Data = read.table("student-mat.csv",sep=";",header=TRUE)

#"GP" - Gabriel Pereira or "MS" - Mousinho da Silveira
Data$school <- recode(Data$school, "GP"="1", "MS"="2") 
Data$school <- as.integer(Data$school)

Data$sex <- recode(Data$sex, "M"="1", "F"="2") 
Data$sex <- as.integer(Data$sex)

#U-urban, R-rural
Data$address <- recode(Data$address, "U"="1", "R"="2") 
Data$address <- as.integer(Data$address)

#"LE3" - less or equal to 3 or "GT3" - greater than 3
Data$famsize <- recode(Data$famsize, "LE3"="1", "GT3"="2") 
Data$famsize <- as.integer(Data$famsize)

#Parents: "T" - living together or "A" - apart
Data$Pstatus <- recode(Data$Pstatus, "T"="1", "A"="2") 
Data$Pstatus <- as.integer(Data$Pstatus)

#"teacher", "health" care related, civil "services" (e.g. administrative or police), "at_home" or "other"
Data$Mjob <- recode(Data$Mjob, "teacher"="1", "health"="2", "services" = "3", "at_home"="4", "other"="5") 
Data$Mjob <- as.integer(Data$Mjob)

#"teacher", "health" care related, civil "services" (e.g. administrative or police), "at_home" or "other"
Data$Fjob <- recode(Data$Fjob, "teacher"="1", "health"="2", "services" = "3", "at_home"="4", "other"="5") 
Data$Fjob <- as.integer(Data$Fjob)

#reason to choose this school: close to "home", school "reputation", "course" preference or "other"
Data$reason <- recode(Data$reason, "home"="1", "reputation"="2", "course" = "3","other"="4") 
Data$reason <- as.integer(Data$reason)

#student's guardian: "mother", "father" or "other")
Data$guardian <- recode(Data$guardian, "mother"="1", "father"="2", "other"="3") 
Data$guardian <- as.integer(Data$guardian)

#extra educational support: 1-yes, 2-no
Data$schoolsup <- recode(Data$schoolsup, "yes"="1", "no"="2") 
Data$schoolsup <- as.integer(Data$schoolsup)

#family educational support: 1-yes, 2-no
Data$famsup <- recode(Data$famsup, "yes"="1", "no"="2") 
Data$famsup <- as.integer(Data$famsup)

#extra paid classes within the course subject: 1-yes, 2-no
Data$paid <- recode(Data$paid, "yes"="1", "no"="2") 
Data$paid <- as.integer(Data$paid)

# extra-curricular activities: 1-yes, 2-no
Data$activities <- recode(Data$activities, "yes"="1", "no"="2") 
Data$activities <- as.integer(Data$activities)

#attended nursery school: 1-yes, 2-no
Data$nursery <- recode(Data$nursery, "yes"="1", "no"="2") 
Data$nursery <- as.integer(Data$nursery)

#wants to take higher education: 1-yes, 2-no
Data$higher <- recode(Data$higher, "yes"="1", "no"="2") 
Data$higher <- as.integer(Data$higher)

#Internet access at home: 1-yes, 2-no
Data$internet <- recode(Data$internet, "yes"="1", "no"="2") 
Data$internet <- as.integer(Data$internet)

#with a romantic relationship: 1-yes, 2-no
Data$romantic <- recode(Data$romantic, "yes"="1", "no"="2") 
Data$romantic <- as.integer(Data$romantic)

write.csv(Data,"student-math-clean.csv")

g3 <- Data$G3
logicalResults <- as.integer(g3 >= 10)

barplot(table(logicalResults),
        main="Zaliczenie egzaminu finałowego",
        names.arg = c("Brak zaliczenia", "Zaliczenie"),
        col = "steelblue")
hist(g3, 
     main ="Histogram punktów za egzamin końcowy",
     ylab = "",
     xlab = "",
     col = "steelblue")

hist(g3, 
     main ="Histogram punktów za egzamin końcowy",
     ylab = "",
     xlab = "",
     col = "steelblue")

hist(g3,
     freq = FALSE,
     main ="Histogram gęstości punktów z egzaminu końcowego",
     xlab = "",
     ylab = "",
     col = "steelblue")
lines(density(g3), col = "red")

#Q-Q Plot z wykorzystaniem wygenerowanych 
#liczb z rozkładu normalnego (nie zapomnieć o set.seed())
#bo rnorm może się różnić
set.seed(11)
qqplot(rnorm(50, 50, 2),
       g3,
       xlab = "Rozkład normalny",
       ylab = "Punkty z egzaminu",
       main = "Q-Q Plot",
       col = "steelblue")

qqnorm(g3,
       main = "Normal Q-Q Plot",
       xlab = "Teoretyczny", 
       ylab = "Punkty z egzmainu", 
       col = "steelblue")
qqline(g3, col = "red", lwd = 2)

normality_test<-function(Data)
{
  los = 30 #ilosc elementow do wylosowania 
  
  sample_school <- sample(Data$school, size = los, replace = FALSE, prob=NULL)
  
  sample_sex <- sample(Data$sex, size = los, replace = FALSE, prob=NULL)
  
  sample_age <- sample(Data$age, size = los, replace = FALSE, prob=NULL)
  
  sample_address <- sample(Data$address, size = los, replace = FALSE, prob=NULL)
  
  sample_famsize <- sample(Data$famsize, size = los, replace = FALSE, prob=NULL)
  
  sample_Pstatus <- sample(Data$Pstatus, size = los, replace = FALSE, prob=NULL)
  
  sample_Medu <- sample(Data$Medu, size = los, replace = FALSE, prob=NULL)
  
  sample_Fedu <- sample(Data$Fedu, size = los, replace = FALSE, prob=NULL)
  
  sample_Mjob <- sample(Data$Mjob, size = los, replace = FALSE, prob=NULL)
  
  sample_Fjob <- sample(Data$Fjob, size = los, replace = FALSE, prob=NULL)
  
  sample_reason <- sample(Data$reason, size = los, replace = FALSE, prob=NULL)
  
  sample_guardian <- sample(Data$guardian, size = los, replace = FALSE, prob=NULL)
  
  sample_traveltime <- sample(Data$traveltime, size = los, replace = FALSE, prob=NULL)
  
  sample_studytime <- sample(Data$studytime, size = los, replace = FALSE, prob=NULL)
  
  sample_failures <- sample(Data$failures, size = los, replace = FALSE, prob=NULL)
  
  sample_schoolsup <- sample(Data$schoolsup, size = los, replace = FALSE, prob=NULL)
  
  sample_famsup <- sample(Data$famsup, size = los, replace = FALSE, prob=NULL)
  
  sample_paid <- sample(Data$paid, size = los, replace = FALSE, prob=NULL)
  
  sample_activities <- sample(Data$activities, size = los, replace = FALSE, prob=NULL)
  
  sample_nursery <- sample(Data$nursery, size = los, replace = FALSE, prob=NULL)
  
  sample_higher <- sample(Data$higher, size = los, replace = FALSE, prob=NULL)
  
  sample_internet <- sample(Data$internet, size = los, replace = FALSE, prob=NULL)
  
  sample_romantic <- sample(Data$romantic, size = los, replace = FALSE, prob=NULL)
  
  sample_famrel <- sample(Data$famrel, size = los, replace = FALSE, prob=NULL)
  
  sample_freetime <- sample(Data$freetime, size = los, replace = FALSE, prob=NULL)
  
  sample_goout <- sample(Data$goout, size = los, replace = FALSE, prob=NULL)
  
  sample_Dalc <- sample(Data$Dalc, size = los, replace = FALSE, prob=NULL)
  
  sample_Walc <- sample(Data$Walc, size = los, replace = FALSE, prob=NULL)
  
  sample_health <- sample(Data$health, size = los, replace = FALSE, prob=NULL)
  
  sample_absences <- sample(Data$absences, size = los, replace = FALSE, prob=NULL)
  
  sample_G1<- sample(Data$G1, size = los, replace = FALSE, prob=NULL)
  
  sample_G2<- sample(Data$G2, size = los, replace = FALSE, prob=NULL)
  
  sample_G3<- sample(Data$G3, size = los, replace = FALSE, prob=NULL)
  
  
  #sink(file = "normalized-test/Test_Normalnosci_Shapiro_Wilka.txt", append = FALSE)
  
  shapiro.test(sample_school)
  shapiro.test(sample_sex)
  shapiro.test(sample_age)
  shapiro.test(sample_address)
  shapiro.test(sample_famsize)
  shapiro.test(sample_Pstatus)
  shapiro.test(sample_Medu)
  shapiro.test(sample_Fedu)
  shapiro.test(sample_Mjob)
  shapiro.test(sample_Fjob)
  shapiro.test(sample_reason)
  shapiro.test(sample_guardian)
  shapiro.test(sample_traveltime)
  shapiro.test(sample_studytime)
  shapiro.test(sample_failures)
  shapiro.test(sample_schoolsup)
  shapiro.test(sample_famsup)
  shapiro.test(sample_paid)
  shapiro.test(sample_activities)
  shapiro.test(sample_nursery)
  shapiro.test(sample_higher) #wszystkie wartości 'x' są identyczne
  shapiro.test(sample_internet)
  shapiro.test(sample_romantic)
  shapiro.test(sample_famrel)
  shapiro.test(sample_freetime)
  shapiro.test(sample_goout)
  shapiro.test(sample_Dalc)
  shapiro.test(sample_Walc)
  shapiro.test(sample_health)
  shapiro.test(sample_absences)
  shapiro.test(sample_G1)
  shapiro.test(sample_G2)
  shapiro.test(sample_G3)
  #sink()
  
  #sink(file = "normalized-test/Test-Normalnosci-Pearsona-Chi^2.txt",
  #     append = FALSE)
  pearson.test(sample_school,  adjust = TRUE)
  pearson.test(sample_sex,  adjust = TRUE)
  pearson.test(sample_age,  adjust = TRUE)
  pearson.test(sample_address,  adjust = TRUE)
  pearson.test(sample_famsize,  adjust = TRUE)
  pearson.test(sample_Pstatus,  adjust = TRUE)
  pearson.test(sample_Medu,  adjust = TRUE)
  pearson.test(sample_Fedu,  adjust = TRUE)
  pearson.test(sample_Mjob,  adjust = TRUE)
  pearson.test(sample_Fjob,  adjust = TRUE)
  pearson.test(sample_reason,  adjust = TRUE)
  pearson.test(sample_guardian,  adjust = TRUE)
  pearson.test(sample_traveltime,  adjust = TRUE)
  pearson.test(sample_studytime,  adjust = TRUE)
  pearson.test(sample_failures,  adjust = TRUE)
  pearson.test(sample_schoolsup,  adjust = TRUE)
  pearson.test(sample_famsup,  adjust = TRUE)
  pearson.test(sample_paid,  adjust = TRUE)
  pearson.test(sample_activities,  adjust = TRUE)
  pearson.test(sample_nursery,  adjust = TRUE)
  pearson.test(sample_higher,  adjust = TRUE)
  pearson.test(sample_internet,  adjust = TRUE)
  pearson.test(sample_romantic,  adjust = TRUE)
  pearson.test(sample_famrel,  adjust = TRUE)
  pearson.test(sample_freetime,  adjust = TRUE)
  pearson.test(sample_goout,  adjust = TRUE)
  pearson.test(sample_Dalc,  adjust = TRUE)
  pearson.test(sample_Walc,  adjust = TRUE)
  pearson.test(sample_health,  adjust = TRUE)
  pearson.test(sample_absences,  adjust = TRUE)
  pearson.test(sample_G1,  adjust = TRUE)
  pearson.test(sample_G2,  adjust = TRUE)
  pearson.test(sample_G3,  adjust = TRUE)
  #sink()
  
  #sink(file = "normalized-test/Test_Normalnosci_Shapiro-Francia.txt", append = FALSE)
  sf.test(sample_school)
  sf.test(sample_sex)
  sf.test(sample_age)
  sf.test(sample_address)
  sf.test(sample_famsize)
  sf.test(sample_Pstatus)
  sf.test(sample_Medu)
  sf.test(sample_Fedu)
  sf.test(sample_Mjob)
  sf.test(sample_Fjob)
  sf.test(sample_reason)
  sf.test(sample_guardian)
  sf.test(sample_traveltime)
  sf.test(sample_studytime)
  sf.test(sample_failures)
  sf.test(sample_schoolsup)
  sf.test(sample_famsup)
  sf.test(sample_paid)
  sf.test(sample_nursery)
  sf.test(sample_higher)
  sf.test(sample_internet)
  sf.test(sample_romantic)
  sf.test(sample_famrel)
  sf.test(sample_freetime)
  sf.test(sample_goout)
  sf.test(sample_Dalc)
  sf.test(sample_Walc)
  sf.test(sample_health)
  sf.test(sample_absences)
  sf.test(sample_G1)
  sf.test(sample_G2)
  sf.test(sample_G3)
  #sink()
}



